import java.io.*;
import java.util.*;

public class gameTree {
	
	/* @player : true : player1    false : player2
	 * @winner : 1 : player1 wins	2 : player2 wins 	3 : no winner
	 * @board : current board;
	 * @next : collection of cases after next move;
	 */	
	private boolean player;
	public int winner = -1;
	public char[][] board;
	public List<gameTree> next;
	
	public gameTree(boolean player, char[][] b){
		this.player = player;
		board = new char[3][3];
		next = new ArrayList();
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				this.board[i][j] = b[i][j];				
			}
		}		
	}
	public boolean getPlayer(){
		return player;
	}
	public char[][] getBoard(){
		return board;
	}
	public List<gameTree> getAllNextCase(){
		return next;
	}
	
	//Make a move
	public gameTree putStone(int row, int col, String role){
		if(row < 0 || row > 2
		|| col < 0 || row > 2
		|| board[row][col] != '_'){
			System.out.println("Invalid position!");
			return this;
		}
		if(role.equals("X"))
			board[row][col] = (player ? 'X' : 'O');
		else
			board[row][col] = (player ? 'O' : 'X');
		gameTree maybe = new gameTree(!player, getBoard());
		board[row][col] = '_';
		return maybe;
	}
	
	//Generate the collection of next
	public void generateNext(String role){
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(board[i][j] != 'X' && board[i][j] != 'O'){
					gameTree maybe = putStone(i, j, (role));
					next.add(maybe);
				}
			}
		}
		for(gameTree n : next){
			n.generateNext(role);
		}
	}
	//To conclude whether the game ends
	public boolean terminalTest(){
		
		//All case for 'X' to win
		if(board[1][1] == 'X'){
			if((board[0][0] == 'X' && board[2][2] == 'X')
			|| (board[0][1] == 'X' && board[2][1] == 'X')
			|| (board[0][2] == 'X' && board[2][0] == 'X')
			|| (board[1][0] == 'X' && board[1][2] == 'X')){
				winner = 1;
				return true;
			}
		}
		if(board[0][0] == 'X'){
			if((board[0][1] == 'X' && board[0][2] == 'X')
			|| (board[1][0] == 'X' && board[2][0] == 'X')){
				winner = 1;
				return true;
			}				
		}
		if(board[2][2] == 'X'){
			if((board[2][1] == 'X' && board[2][0] == 'X')
			|| (board[1][2] == 'X' && board[0][2] == 'X')){
				winner = 1;
				return true;
			}				
		}
		
		//All case for 'O' to win
		if(board[1][1] == 'O'){
			if((board[0][0] == 'O' && board[2][2] == 'O')
			|| (board[0][1] == 'O' && board[2][1] == 'O')
			|| (board[0][2] == 'O' && board[2][0] == 'O')
			|| (board[1][0] == 'O' && board[1][2] == 'O')){
				winner = 2;
				return true;
			}							
		}
		if(board[0][0] == 'O'){
			if((board[0][1] == 'O' && board[0][2] == 'O')
			|| (board[1][0] == 'O' && board[2][0] == 'O')){
				winner = 2;
				return true;
			}				
		}
		if(board[2][2] == 'O'){
			if((board[2][1] == 'O' && board[2][0] == 'O')
			|| (board[1][2] == 'O' && board[0][2] == 'O')){
				winner = 2;  
				return true;
			}				
		}
		
		//Tie
		int cnt = 0;
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(board[i][j] == 'X' || board[i][j] == 'O') cnt++;
			}
			if(cnt == 9) return true;
		}
		
		//incomplete game
		return false;
	}
	public double utility(){
		
		// In particular player's turn, if the game ends, this player wins
		if(this.terminalTest()){
			if(this.winner != 1 && this.winner != 2) return 0.5;
			else return 1.0;
		}
		
		double max = 0.0;
		int size = next.size();
		for(int i = 0; i < size; i++){
			max = Math.max(max, next.get(i).utility());
		}
		return 1.0 - max;
	}
	
	//Show the board
	public void printBoard(){
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				System.out.print(board[i][j] + " ");
			}
			System.out.print('\n');
		}
	}
	public void printWinner(String role){
		if(role.equals("X")){
			switch(winner){
				case 1 : System.out.println("You win!"); break;
				case 2 : System.out.println("Computer win!"); break;
				default : System.out.println("Tie!");
			}
		}
		else{
			switch(winner){
				case 2 : System.out.println("You win!"); break;
				case 1 : System.out.println("Computer win!"); break;
				default : System.out.println("Tie!");
			}
		}
	}
}
