import java.util.*;
import java.io.*;

public class play {
	public static void main(String[] args){
		while(true){
			System.out.println("----New game----");
			char[][] b = new char[3][3];
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					b[i][j] = '_';				
				}
			}
			Scanner in = new Scanner(System.in);
			Random rand = new Random();
			gameTree game;

			System.out.print("Choose your stone ('X' / 'O'): ");
			String role = in.nextLine().toUpperCase();
			if(role.equals("X")){			
				game = new gameTree(true, b);
				game.printBoard();
			}
			else if(role.equals("O"))
				game = new gameTree(false, b);
			else{
				System.out.println("----Game ends----");
				return;
			}
			
			game.generateNext(role);					
			
			while(game.terminalTest() == false){
				
				//player's move
				if(game.getPlayer()){
					System.out.print("Press 1 - 9 to place your stone, 0 to exit : ");
					int pos, row, col;
					do{
						pos = in.nextInt();
						if(pos == 0){
							System.out.println("----Game ends----");
							return;
						}
						row = (pos - 1) / 3;
						col = (pos - 1) % 3;
						if(game.board[row][col] != '_')
							System.out.println("Invalid position!! Try again : ");
					}while(game.board[row][col] != '_');
					
					int cnt = 0;
					int cntPos = 1;
					for(int i = 0; i < 3; i++){
						for(int j = 0; j < 3; j++){
							if(cntPos < pos){
								cntPos++;
								if(game.board[i][j] == '_')
									cnt++;
							}
							else break;
						}
						if(cntPos >= pos) break;
					}
					game = game.next.get(cnt);
				}
									
				//computer's move
				else{
					List<gameTree> gameNext= game.getAllNextCase();
					double maxValue = -1.0;
					List<gameTree> maxes = new ArrayList();
					
					for(gameTree maybe : gameNext){
						if(maybe.utility() == maxValue){
							maxes.add(maybe);
						}
						else if(maybe.utility() > maxValue){
							maxes.clear();
							maxes.add(maybe);
							maxValue = maybe.utility();
						}
					}
					game = maxes.get(rand.nextInt(maxes.size()));
					game.printBoard();
				}
			}
			game.printWinner(role);
			game.printBoard();
		}
	}
}
